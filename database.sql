create database if not exists geomant_test;
use geomant_test;

drop table if exists X;
create table X(
    IDX INT auto_increment primary key,
    FirstName VARCHAR(50),
    LastName VARCHAR(50),
    EMail VARCHAR(30),
    PhoneNumber VARCHAR(15),
    ModifiedDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

insert into X (FirstName, LastName, EMail, PhoneNumber) values ('Szekeres', 'Anna', 'annaszekeres@gmail.com', '0764589612');
insert into X (FirstName, LastName, EMail, PhoneNumber) values ('Smith', 'John', 'smithy@gmail.com', '035645635295');
insert into X (FirstName, LastName, EMail, PhoneNumber) values ('Luca', 'Gaal', 'luca@gmail.com', '0786549874');
insert into X (FirstName, LastName, EMail, PhoneNumber) values ('Anne', 'Frank', 'frananne@gmail.com', '036596857485');
insert into X (FirstName, LastName, EMail, PhoneNumber) values ('Benedek', 'Elek', 'elekbenedek@gmail.com', '0764568612');

select * from X;

DROP PROCEDURE IF EXISTS GetX;
DELIMITER ;;
CREATE PROCEDURE GetX(IN p_idx INT)
BEGIN
	IF p_idx IS NULL THEN
		SELECT * FROM `X` ORDER BY ModifiedDate DESC;
	else
		SELECT * FROM `X` WHERE idx = p_idx;
	end if;
END;;
DELIMITER ;


insert into X(FirstName, LastName, EMail, PhoneNumber) values("Alma", "Pinokkio", "pini@yahoo.com", "123456789");
/*CALL GetX(null);
CALL GetX(1);*/