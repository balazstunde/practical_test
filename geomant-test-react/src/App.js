import React from "react";
import "./App.css";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";

class InputAndTableArea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // the input field content (this will be the filter id)
      idx: null,
      // the table content
      xs: [],
      // if the input field contains other carachters than alphabetical, then I have to show an error label
      showError: false
    };
  }

  async componentDidMount() {
    this.refreshXs();
    // check in every 5 seconds  after some changes in database
    setInterval(this.refreshXs, 5000);
  }

  // handle the input field changes
  handleChange = e => {
    // only numbers are accepted
    const re = /^[0-9\b]+$/;

    // if value is not blank, then test the regex
    if (e.target.value === "" || re.test(e.target.value)) {
      this.setState({ idx: e.target.value, showError: false });
    } else {
      this.setState({ showError: true });
    }
  };

  // refresh the table when execute was clicked
  refreshTable = e => {
    console.log(this.state.idx);
    this.refreshXs();
  };

  // refresh the component's state by getting the xml response from the server
  refreshXs = () => {
    // if the idx is null or it's empty we want to get the full table, so we will call the /xs endpoint
    if (this.state.idx === null || this.state.idx === "") {
      axios.get("http://localhost:8080/xs/").then(response => {
        this.setState({
          xs: response.data
        });
        console.log(response.data);
      });
    } else {
      // otherwise we want to get just one element by id
      axios.get("http://localhost:8080/xs/" + this.state.idx).then(response => {
        this.setState({
          xs: response.data
        });
        console.log(response.data);
      });
    }
  };

  // render the table by the xs content
  renderTableData() {
    return this.state.xs.map((x, index) => {
      const { id, firstName, lastName, email, phoneNumber } = x; //destructuring
      return (
        <tr key={id}>
          <td>{firstName}</td>
          <td>{lastName}</td>
          <td>{email}</td>
          <td>{phoneNumber}</td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div>
        <div className="input-group mb-3 text-center">
          <div className="input-group-append">
            <input
              type="text"
              className="form-control"
              placeholder="IDX"
              aria-describedby="basic-addon2"
              onChange={this.handleChange}
            ></input>

            <button
              className="btn btn-outline-secondary"
              type="button"
              onClick={this.refreshTable}
            >
              Execute
            </button>
          </div>
        </div>
        {this.state.showError ? (
          <div className="alert alert-warning" role="alert">
            Hmmm.. The idx can be only numerical.
          </div>
        ) : null}
        <table className="table table-hover table-dark">
          <thead>
            <tr>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Email</th>
              <th scope="col">Phone Number</th>
            </tr>
          </thead>
          <tbody>{this.renderTableData()}</tbody>
        </table>
      </div>
    );
  }
}

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <InputAndTableArea />
      </div>
    );
  }
}

export default App;
