package edu.geomant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.geomant.model.X;
import edu.geomant.service.XService;

@RestController
public class MainController {

	@Autowired
	private XService xService;
	
	@RequestMapping("/xs")
	public List<X> getXs() {
		return xService.getXs(null);
	}
	
	@RequestMapping("/xs/{idx}")
	public List<X> getXsWithParam(@PathVariable Integer idx) {
		return xService.getXs(idx);
	}
}
