package edu.geomant.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.geomant.model.X;
import edu.geomant.repository.XRepository;

@Service
public class XService {

	@Autowired
	private XRepository xRepository;
	
	public List<X> getXs(Integer idx) {
		return xRepository.getX(idx);
	}
}
