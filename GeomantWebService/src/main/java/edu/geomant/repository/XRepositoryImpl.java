package edu.geomant.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import edu.geomant.model.X;

public class XRepositoryImpl implements XRepositoryCustom{
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<X> getX(Integer idx) {
		StoredProcedureQuery findXs = em.createNamedStoredProcedureQuery("getX");
		findXs.setParameter("p_idx", idx);
	    return findXs.getResultList();
	}
	
	
}
