package edu.geomant.repository;

import java.util.List;

import edu.geomant.model.X;

public interface XRepositoryCustom {
	List<X> getX(Integer idx);
}
