
public class Algorithms {
	
	private static String duplicateLetters(String word) {
		return word.replaceAll(".", "$0$0");
	}
	
	private static void printNumbersFrom1To100() {
		for(Integer i = 1; i <= 100; i++) {
			if(i % 3 == 0) {
				System.out.print("Fizz");
				if(i % 5 == 0) {
					System.out.print("Buzz");
				}
				System.out.println("");
			}else {
				if(i % 5 == 0) {
					System.out.println("Buzz");
				}else {
					System.out.println(i);
				}
			}
		}
	}
	
	private static void printNumbersFrom1To100a() {
		for(Integer i = 1; i <= 100; i++) {
			if(i % 15 == 0) {
				System.out.println("FizzBuzz");
			}else {
				if(i % 3 == 0) {
					System.out.println("Fizz");
				}else {
					if(i % 5 == 0) {
						System.out.println("Buzz");
					}else {
						System.out.println(i);
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		System.out.println(duplicateLetters("alma"));
		printNumbersFrom1To100();
		printNumbersFrom1To100a();
	}

}
